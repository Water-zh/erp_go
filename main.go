package main

import (
	"net/http"

	"gopkg.in/gin-gonic/gin.v1"
)

func main() {
	mGin := gin.Default()
	mGin.LoadHTMLGlob("views/*")
	mGin.Static("/public", "./public")

	mGin.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/about-us", func(c *gin.Context) {
		c.HTML(http.StatusOK, "aboutus.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/products", func(c *gin.Context) {
		c.HTML(http.StatusOK, "products.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/products2", func(c *gin.Context) {
		c.HTML(http.StatusOK, "products2.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/sourcing", func(c *gin.Context) {
		c.HTML(http.StatusOK, "sourcing.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/led", func(c *gin.Context) {
		c.HTML(http.StatusOK, "led.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.GET("/contact-us", func(c *gin.Context) {
		c.HTML(http.StatusOK, "contact-us.html", gin.H{
			"title": "Summer-Wind",
		})
	})
	mGin.Run(":8080")
}
